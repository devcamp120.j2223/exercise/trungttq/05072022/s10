import Datatable from "./components/Datatable";

function App() {
  return (
    <div>
      <Datatable />
    </div>
  );
}

export default App;
