import { Container, Grid, TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Button, Pagination } from "@mui/material";
import { useEffect, useState } from "react";

function Datatable() {
    const [rows, setRows] = useState([]);

    // Limit: Số lượng bản ghi trên 1 trang
    const limit = 10;

    // Số trang: Tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
    const [noPage, setNoPage] = useState(0);

    // Trang hiện tại
    const [page, setPage] = useState(1);

    const getData = async (url) => {
        const response = await fetch(url);

        const data = await response.json();

        return data;
    }

    const clickHandler = (userInfo) => {
        console.log(userInfo);
    }

    const onChangePagination = (event, value) => {
        setPage(value);
    }

    useEffect(() => {
        getData("https://jsonplaceholder.typicode.com/posts")
            .then((res) => {
                setNoPage(Math.ceil(res.length / limit));

                setRows(res.slice( (page - 1) * limit, page * limit ));
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [page]);

    return (
        <Container>
            <Grid container>
                <Grid item>
                    <TableContainer>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">ID</TableCell>
                                    <TableCell align="center">User ID</TableCell>
                                    <TableCell align="center">Title</TableCell>
                                    <TableCell align="center">Body</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    rows.map((row, index) => {
                                        return (
                                            <TableRow key={index}>
                                                <TableCell align="center">{row.id}</TableCell>
                                                <TableCell align="center">{row.userId}</TableCell>
                                                <TableCell>{row.title}</TableCell>
                                                <TableCell>{row.body}</TableCell>
                                                <TableCell align="center"><Button variant="contained" onClick={() => {clickHandler(row)}}>Chi tiết</Button></TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container mt={3} mb={2} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage} defaultPage={page} onChange={onChangePagination} />
                </Grid>
            </Grid>
        </Container>
    )
}

export default Datatable;